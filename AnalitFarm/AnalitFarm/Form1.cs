﻿using System;
using System.Collections.Generic;

using System.Windows.Forms;

namespace AnalitFarm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            UI.ProductsListBox = listBox1;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Добавляем новые продукты в список
            ApplicationManager AppManager = new ApplicationManager();
            AppManager.AddProduct("Мороженое1", 30, 2000);
            AppManager.AddProduct("Мороженое2", 100, 1000);
            AppManager.AddProduct("Мороженое3", 300, 1000);
        }
        //при смене выбранного продукта
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Преобразуем для получения инфы
            ApplicationManager.productUnit doc = (ApplicationManager.productUnit)listBox1.Items[listBox1.SelectedIndex];
            ProductTitle.Text = doc.item.Title;
            ProductID.Text = "Код: " + doc.productID.ToString();
            ProductPrice.Text = "Цена: " + doc.item.Price.ToString();
            ProductAmount.Text = "Кол-во: " + doc.amount.ToString();
            ProductSum.Text = "Сумма: " + (doc.amount * doc.item.Price).ToString();
            
        }
    }
    class ApplicationManager
    {
        private static ApplicationManager instance;//singleton
        public struct productUnit
        {
            public Item item;
            public int amount;
            public int productID;
            //Переопределяем, чтобы нормально выводилось в список продуктов
            public override string ToString()
            {
                return string.Format("{0}{1,100}{2,40}", item.Title, amount, item.Price * amount);
            }
        }
        private List<productUnit> products = new List<productUnit>();
        
        /// <summary>
        /// Добавление нового продукта
        /// </summary>
        /// <param name="title">Название продукта</param>
        /// <param name="price">Цена продукта</param>
        /// <param name="_amount">Кол-во продукта</param>
        public void AddProduct(string title, double price, int _amount = 1)
        {
            productUnit newDocument = new productUnit { item = new Item(title, price), amount = _amount, productID = products.Count};
            products.Add(newDocument);
            UI.DrawNewProduct(newDocument);
        }
        //Удаление продукта
        public void RemoveProduct(string title, double price, int _amount)
        {
            foreach (var curProduct in products)
            {
                if (curProduct.item.Title == title && curProduct.item.Price == price)
                {
                    //if(curProduct.amount < _amount) //ошибка
                }
            }
        }
        public ApplicationManager() {}
        
        //Одиночка
        public static ApplicationManager Instance
        {
            get
            {
                if(instance == null)
                {
                    instance = new ApplicationManager();
                }
                return instance;
            }
        }
    }
    /// <summary>
    /// Класс, отрисовывающий UI
    /// </summary>
    static class UI
    {
        private static ListBox productsListBox;
        public static ListBox ProductsListBox
        {
            set
            {
                if(productsListBox == null)
                {
                    productsListBox = value;
                }
            }
        }
        public static void DrawNewProduct(ApplicationManager.productUnit doc)
        {
            productsListBox.Items.Add(doc);
        }

    }
    class Item
    {
        string title;
        double price;
        public string Title
        {
            get { return title; }
        }
        public double Price
        {
            get { return price; }
        }
        public Item(string title, double price)
        {
            this.title = title;
            this.price = price;
        }
    }
    
}
